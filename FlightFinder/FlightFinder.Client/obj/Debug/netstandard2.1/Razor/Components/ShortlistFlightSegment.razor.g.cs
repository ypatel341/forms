#pragma checksum "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d3a0b0153035d2b2361610498b5793e6d1aab9fe"
// <auto-generated/>
#pragma warning disable 1591
namespace FlightFinder.Client.Components
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using FlightFinder.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using FlightFinder.Client.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using FlightFinder.Shared;

#line default
#line hidden
#nullable disable
    public partial class ShortlistFlightSegment : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "date");
            __builder.AddMarkupContent(2, "\n    ");
            __builder.OpenElement(3, "h4");
            __builder.AddContent(4, 
#nullable restore
#line 2 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor"
         Flight.DepartureTime.ToString("ddd MMM d")

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(5, "\n    ");
            __builder.AddContent(6, 
#nullable restore
#line 3 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor"
     Flight.TicketClass.ToDisplayString()

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(7, "\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(8, "\n\n");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "class", "departure");
            __builder.AddMarkupContent(11, "\n    ");
            __builder.OpenElement(12, "h4");
            __builder.AddContent(13, 
#nullable restore
#line 7 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor"
         Flight.DepartureTime.ToShortTimeString()

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(14, "\n    ");
            __builder.AddContent(15, 
#nullable restore
#line 8 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor"
     Flight.FromAirportCode

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(16, "\n");
            __builder.CloseElement();
            __builder.AddMarkupContent(17, "\n\n");
            __builder.AddMarkupContent(18, "<div class=\"arrow\">➝</div>\n\n");
            __builder.OpenElement(19, "div");
            __builder.AddAttribute(20, "class", "return");
            __builder.AddMarkupContent(21, "\n    ");
            __builder.OpenElement(22, "h4");
            __builder.AddContent(23, 
#nullable restore
#line 14 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor"
         Flight.ReturnTime.ToShortTimeString()

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(24, "\n    ");
            __builder.AddContent(25, 
#nullable restore
#line 15 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor"
     Flight.ToAirportCode

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(26, "\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 19 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/ShortlistFlightSegment.razor"
 
    [Parameter]
    public FlightSegment Flight { get; set; }

#line default
#line hidden
#nullable disable
    }
}
#pragma warning restore 1591
