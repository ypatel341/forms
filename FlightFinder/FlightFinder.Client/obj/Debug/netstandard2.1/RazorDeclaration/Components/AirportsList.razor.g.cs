#pragma checksum "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/AirportsList.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "58c0d24b67c47015101bd36f79ad366b059f217c"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace FlightFinder.Client.Components
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using FlightFinder.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using FlightFinder.Client.Components;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/_Imports.razor"
using FlightFinder.Shared;

#line default
#line hidden
#nullable disable
    public partial class AirportsList : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 11 "/Users/ypatel14/Projects/blazor/FlightFinder/FlightFinder.Client/Components/AirportsList.razor"
 
    private Airport[] airports = Array.Empty<Airport>();

    protected override async Task OnInitializedAsync()
    {
        airports = await Http.GetJsonAsync<Airport[]>("api/airports");
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private HttpClient Http { get; set; }
    }
}
#pragma warning restore 1591
