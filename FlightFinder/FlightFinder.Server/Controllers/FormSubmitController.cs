﻿using FlightFinder.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FlightFinder.Server.Controllers
{
    [Route("api/[controller]")]
    public class FormSubmitController
    {
        public string Submit([FromBody] FullFormRequest fullForm)
        {
            Console.WriteLine(fullForm);
            return "blank";
        }
    }
}
