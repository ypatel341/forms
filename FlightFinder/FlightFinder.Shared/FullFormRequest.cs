﻿using System;
namespace FlightFinder.Shared
{
    public class FullFormRequest
    {
        public string Name { get; set; }
        public DateTime RequestDate { get; set; }
        public string DepartmentLead { get; set; }
        public string Email { get; set; }
        public string SevaDepartmnet { get; set; }
        public string DepartmentAdmin { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }

        public FullFormRequest()
        {
        }
    }
}
